package esco.setup;

import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;

public class Initializer {

    private GraphTraversalSource g;
    private String occupationsFilePath;
    private int occupationUriPos;
    private int occupationNamePos;
    private String skillsFilePath;
    private int skillUriPos;
    private int skillNamePos;
    private String skillsToOccupationRelationsFilePath;
    private int stoSkillUriPos;
    private int stoOccupationUriPos;
    private int stoEdgePos;

    public Initializer(GraphTraversalSource g,
                       String occupationsFilePath, int occupationUriPos, int occupationNamePos,
                       String skillsFilePath, int skillUriPos, int skillNamePos,
                       String occupationSkillsRelationsFilePath, int occupationSkillFromPos, int occupationSkillToPos, int occupationSkillEdgePos) {
        this.g = g;
        this.occupationsFilePath = occupationsFilePath;
        this.occupationUriPos = occupationUriPos;
        this.occupationNamePos = occupationNamePos;
        this.skillsFilePath = skillsFilePath;
        this.skillUriPos = skillUriPos;
        this.skillNamePos = skillNamePos;
        this.skillsToOccupationRelationsFilePath = occupationSkillsRelationsFilePath;
        this.stoSkillUriPos = occupationSkillFromPos;
        this.stoOccupationUriPos = occupationSkillToPos;
        this.stoEdgePos = occupationSkillEdgePos;
    }

    private static final String NODE_TYPE = "nodeType";
    private static final String URI = "uri";
    private static final String NAME = "name";
    private static final String OCCUPATION = "occupation";
    private static final String SKILL = "skill";
    private static final String SKILL_TO_OCCUPATION = "skillToOccupation";
    private static final String SYSTEM = "system";
    private static final String STATUS = "status";
    private static final String INITIALIZED = "initialized";


    public void initialize() throws IOException {
        initializeOccupations();
        initializeSkills();
        initializeSkillsToOccupationsRelations();
    }

    private void initializeOccupations() throws IOException {
        try {
            g.V().has(SYSTEM, NAME, OCCUPATION).has(SYSTEM, STATUS, INITIALIZED).next();
        } catch (NoSuchElementException e) {
            initializeNodes(OCCUPATION, occupationsFilePath, occupationUriPos, occupationNamePos);
            g.addV(SYSTEM).property(NODE_TYPE, SYSTEM).property(NAME, OCCUPATION).property(STATUS, INITIALIZED).next();
        }

    }

    private void initializeSkills() throws IOException {
        try {
            g.V().has(SYSTEM, NAME, SKILL).has(SYSTEM, STATUS, INITIALIZED).next();
        } catch (NoSuchElementException e) {
            initializeNodes(SKILL, skillsFilePath, skillUriPos, skillNamePos);
            g.addV(SYSTEM).property(NODE_TYPE, SYSTEM).property(NAME, SKILL).property(STATUS, INITIALIZED).next();
        }
    }

    private void initializeNodes(String label, String filePath, int uriPosition, int namePosition) throws IOException {
        CSVReader csvReader = getCSVReader(filePath);

        // get rid of table header
        String[] line = csvReader.readNext();
        while ((line = csvReader.readNext()) != null) {
            String uri = line[uriPosition];
            String name = line[namePosition];
            g.addV(label).property(NODE_TYPE, label).property(URI, uri).property(NAME, name).next();
        }
    }

    private void initializeSkillsToOccupationsRelations() throws IOException {
        try {
            g.V().has(SYSTEM, NAME, SKILL_TO_OCCUPATION).has(SYSTEM, STATUS, INITIALIZED).next();
        } catch (NoSuchElementException e) {
            initializeRelations(skillsToOccupationRelationsFilePath, SKILL, URI, stoSkillUriPos, OCCUPATION, URI, stoOccupationUriPos, null, stoEdgePos);
            g.addV(SYSTEM).property(NODE_TYPE, SYSTEM).property(NAME, SKILL_TO_OCCUPATION).property(STATUS, INITIALIZED).next();
        }
    }

    public void initializeRelations(String filePath, String fromNodeType, String fromPropertyName, int fromPos, String toNodeType, String toPropertyName, int toPos, String edge, int edgePosition) throws IOException {
        CSVReader csvReader = getCSVReader(filePath);

        // get rid of table header
        String[] line = csvReader.readNext();
        while ((line = csvReader.readNext()) != null) {
            String currentEdge;
            String fromProperty = line[fromPos];
            String toProperty = line[toPos];
            if (StringUtils.isEmpty(edge)) {
                currentEdge = line[edgePosition];
            } else {
                currentEdge = edge;
            }

            Vertex from = g.V().has(fromNodeType, fromPropertyName, fromProperty).next();
            Vertex to = g.V().has(toNodeType, toPropertyName, toProperty).next();
            g.addE(currentEdge).from(from).to(to).iterate();
        }
    }

    private CSVReader getCSVReader(String filePath) throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(filePath));
        return new CSVReader(reader);
    }
}

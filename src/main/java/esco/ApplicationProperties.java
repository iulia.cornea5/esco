package esco;

public final class ApplicationProperties {

    public static String OCCUPATIONS_FILE_PATH = "C:\\_\\UTCN\\Master IVA\\Disertatie\\esco-app\\src\\main\\resources\\occupations_en.csv";
    public static int OCCUPATIONS_URI_POS = 1;
    public static int OCCUPATIONS_NAME_POS = 3;
    public static String SKILLS_FILE_PATH = "C:\\_\\UTCN\\Master IVA\\Disertatie\\esco-app\\src\\main\\resources\\skills_en.csv";
    public static int SKILLS_URI_POS = 1;
    public static int SKILLS_NAME_POS = 4;
    public static String OCCUPATIONS_SKILLS_RELATIONS_FILE_PATH = "C:\\_\\UTCN\\Master IVA\\Disertatie\\esco-app\\src\\main\\resources\\occupationSkillRelations.csv";
    public static int OCCUPATIONS_SKILLS_RELATIONS_FROM_POS = 3;
    public static int OCCUPATIONS_SKILLS_RELATIONS_TO_POS = 0;
    public static int OCCUPATIONS_SKILLS_RELATIONS_EDGE_POS = 1;
}

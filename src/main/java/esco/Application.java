package esco;

// gremlin-driver module

import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;

// gremlin-core module
import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;

import static esco.ApplicationProperties.*;

import esco.setup.Initializer;

import java.io.IOException;


public class Application {


    public static void main(String[] args) {
        GraphTraversalSource g = traversal().withRemote(
                DriverRemoteConnection.using("localhost", 8182));

        Initializer escoInitializer = new Initializer(g, OCCUPATIONS_FILE_PATH, OCCUPATIONS_URI_POS, OCCUPATIONS_NAME_POS, SKILLS_FILE_PATH, SKILLS_URI_POS, SKILLS_NAME_POS,
                OCCUPATIONS_SKILLS_RELATIONS_FILE_PATH, OCCUPATIONS_SKILLS_RELATIONS_FROM_POS, OCCUPATIONS_SKILLS_RELATIONS_TO_POS, OCCUPATIONS_SKILLS_RELATIONS_EDGE_POS);

        System.out.println("Starting to initialize graph with ESCO data .......");
        try {
            escoInitializer.initialize();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished initializing successfully.");
    }
}

1. Start gremlin server (used apache-tinkerpop-gremlin-server-3.5.2 from https://tinkerpop.apache.org/download.html)
2. Run the application (open in intellij and run Application.main)
3. Start the gremlin visualizer (used https://github.com/prabushitha/gremlin-visualizer)
5. Check out the nodes/edges in the visualizer (ex: g.V())
